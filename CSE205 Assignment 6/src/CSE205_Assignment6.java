/*
CSE 205: <Class 17566> / <Monday/Wednesday 4:35pm>
Assignment: <Assignment 6 > 
Author(s):  <Shashank Ginjpalli> & <1213004310>
Description: <Main Class that Takes Input and Manages menu options >
 */

// importing java util class for the scanner
import java.util.*;

public class CSE205_Assignment6 {
	
	// instance variables for the class
	Scanner scan = new Scanner(System.in);
	private static int menuOption;
	private static String eventName;
	private static String date;
	private static String time;

	public CSE205_Assignment6() {}
	
	// main method 
	public static void main(String[] args) {
		
		// creating the Main class object to call methods in this class
		CSE205_Assignment6 mainObject = new CSE205_Assignment6();
		
		// creating objects for the other classes
		Actions a = new Calendar();
		todoList t = new todoList();
		
		// do while loop to keep printing the menu unless the user exits the program
		do {
			
			// prints out the menu and takes the user input
			mainObject.printMenu();
			mainObject.takeInput();
			
			// switch case that controls that actions of the program
			switch (menuOption) {
			// returns the current date and the time stored on the system
			case 1:
				System.out.println("Current Date and Time:\t " + a.getCurrent());
				break;
			
			// adds an event to the calendar
			case 2:
				mainObject.add();
				a.addEvent(eventName, date, time);
				break;
			
			// removes an event from the calendar 
			case 3:
				mainObject.remove();
				a.removeEvent(eventName);
				break;
			
			// changes the events date and time
			case 4:
				mainObject.change();
				a.changeEvent(eventName, date, time);
				break;
			
			// lists the events on the calendar
			case 5:
				System.out.println(a.listEvents());
				break;

			// adding event to the todo list
			case 6:
				mainObject.addTodo();
				t.addTodo(eventName, false);
				break;
			
			// marks events on the todo list as finished
			case 7:
				t.markAsFinished();
				break;
			
			// lists the events on the toDo list
			case 8:
				System.out.println(t.listEvents());
				break;
			
			// exits the program
			case 9:
				System.out.println("Exiting Calendar");
				System.exit(0);
				break;
			
			// default option just in case nothing is selected
			default:
				System.out.println("Please Select a Valid Option");
				break;
			}

		} while (menuOption != 9);   // repeats the loop as long as the menu option isnt 9

	}

	// method for printing out the menu
	public void printMenu() {
		
		System.out.println("-----------------------------------------------");
		System.out.println("Menu Options");
		System.out.println("Press 1 to get current date and time");
		System.out.println("Press 2 to add events to the calendar");
		System.out.println("Press 3 to remove events from the calendar");
		System.out.println("Press 4 to change events on the Calendar");
		System.out.println("Press 5 to List events");
		System.out.println("Press 6 to Add Events to To-Do List");
		System.out.println("Press 7 To Mark event on To-do List as finished");
		System.out.println("Press 8 To List To-Do List");
		System.out.println("Press 9 to exit program");
		System.out.println("-----------------------------------------------");
		
	}
	
	// takes the input from the user and makes sure its a valid choice
	public void takeInput() {
		
		System.out.println("Please Select an Option");
		menuOption = scan.nextInt();

		while (menuOption < 1 || menuOption > 9) {
			System.out.println("Error Please Enter a Valid Option");
			// recursively asks for input until the user enters a valid option
			takeInput();
		}
		
	}
	
	// prints and prompts for the add to calendar option
	public void add() {
		
		scan.nextLine();
		System.out.print("\n Enter the Name of the Event:\t");
		eventName = scan.nextLine();
		
		System.out.print("\n Enter the Date:\t");
		date = scan.nextLine();
		
		System.out.print("\n Enter the Time:\t");
		time = scan.next();
	}
	
	// propmts and prints for adding to the todo list
	public void addTodo() {
		
		scan.nextLine();
		System.out.print("\n Enter To-Do: \t");
		eventName = scan.nextLine();
	}
	
	// prints and prompts for removing an element from the calendar
	public void remove() {
		
		System.out.print("\n Enter the Name of the Event that you would like to remove: (Spelling Matters): \t");
		eventName = scan.next();
	}
	
	// prints and prompts to change elements on the calendar
	public void change() {
		
		scan.nextLine();
		System.out.print("\n Enter the Name of the Event you would like to change(Spelling Matters):\t");
		eventName = scan.nextLine();

		System.out.print("\n Enter the new Date:\t");
		date = scan.nextLine();
	
		System.out.print("\n Enter the new Time:\t");
		time = scan.next();
	}
	

}
