/*
CSE 205: <Class 17566> / <Monday/Wednesday 4:35pm>
Assignment: <Assignment 6 > 
Author(s):  <Shashank Ginjpalli> & <1213004310>
Description: <Class that implements the To-Do List part of the Program and crosses events off the list >
 */

// class that keeps track of the todoList events 
public class todoList {

	// instance variables for the class
	private todoNode head = null;
	private int index = 0;
	private int numEvents;
	private String listEvents;

	// adds an Item to the ToDo List
	public void addTodo(String eventName, boolean finished) {

		// creating a new Node with the event name and a boolean that keeps track of if
		// it is finished or not
		todoNode todo = new todoNode(eventName, finished);

		// adds the node into the Linked List
		if (head == null) {
			todo.next = head;
			head = todo;
		} else {
			// iterates through the list adding elements
			todoNode current = new todoNode();
			current = head;

			for (int x = 0; x < numEvents - 1; x++) {
				current = current.next;
			}
			// adding to the Linked List
			current.next = todo;
		}
		numEvents++;

	}

	// marks the next element in the linkedList as finished when the user hits the
	// option
	public void markAsFinished() {
		todoNode n = head;

		// makes sure that the first element in the list can still be set to true
		if (index == 0) {
			n.finished = true;
		} else {
			for (int x = 0; x < index; x++) {
				n.finished = true;
				n = n.next;
			}
		}

		// everytime the method runs the index increases by one
		index++;
	}

	// returns all of the elements in the Linked List as a String that is printed in
	// the main class
	public String listEvents() {
		listEvents = "-----------------------------------------------";

		todoNode current = head;

		// Iterates through the list adding to the String
		for (int i = 0; i < numEvents; i++) {
			listEvents = listEvents + "\n" + current.eventName + "\t" + current.finished;
			current = current.next;
		}

		// returns the String back to the Main Class
		return listEvents;
	}
	
}

// class that manages the Nodes for the TodoList
class todoNode {

	// variables that are stored in the List
	String eventName;
	boolean finished;
	todoNode next = null;
	
	public todoNode() {}
	
	public todoNode(String eventName, boolean finished) {
		this.eventName = eventName;
		this.finished = finished;
	}
}
