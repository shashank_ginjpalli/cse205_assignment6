/*
CSE 205: <Class 17566> / <Monday/Wednesday 4:35pm>
Assignment: <Assignment 6 > 
Author(s):  <Shashank Ginjpalli> & <1213004310>
Description: <Interface for the Calendar class with all of the methods that are stored in that class >
 */

// interface for the abstract methods that are used in the Calendar class
public interface Actions {
	
	// list of methods that are overrided by the Calendar class
	public String getCurrent();
	
	public void addEvent(String eventName, String date, String time);
	
	public void removeEvent(String eventName);
	
	public void changeEvent(String eventName, String date, String time);
	
	public String listEvents();
}
