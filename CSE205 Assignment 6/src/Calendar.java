/*
CSE 205: <Class 17566> / <Monday/Wednesday 4:35pm>
Assignment: <Assignment 6 > 
Author(s):  <Shashank Ginjpalli> & <1213004310>
Description: <Calendar class that manages the calendar events and allows to edit events >
 */

// importing time libraries in order to get the current date and time from the system
import java.time.*;
import java.time.format.*;


// class the manages the calendar events and implements the actions interface
public class Calendar implements Actions {
	
	// instance variables for the class
	private String currentDate;
	private Node head = null;
	private int numEvents = 0;
	private String listEvents = "-----------------------------------------------";
	private Boolean exists = true;

	public Calendar() {}
	
	
	// gets the current date and time that is stored on the computer
	@Override
	public String getCurrent() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		
		// computer returns the date and time as a string
		currentDate = dtf.format(now);
		
		// returns the current date and time back to the main class
		return currentDate;
	}
	
	
	// adds the event to the linked list which is storing calendar events
	@Override
	public void addEvent(String eventName, String date, String time) {
		
		// creating a new Node that stores that information
		Node newEvent = new Node(eventName, date, time);
		
		// adding to the linked list
		if (head == null) {
			newEvent.next = head;
			head = newEvent;
		} else {
			Node current = new Node();
			current = head;

			for (int x = 0; x < numEvents - 1; x++) {
				current = current.next;
			}
			current.next = newEvent;
		}
		
		// adding to the number of events to make sure that I can keep track of the number events that I have
		numEvents++;
		System.out.println("Event Has Been Added");
	}
	
	
	// remove an event from the linked list
	@Override
	public void removeEvent(String eventName) {
		// resetting the exists variable to true
		exists = true;
		Node current = head;
		int index = 0;
		
		// iterates through the linked list searching for the index of the event that youre trying to remove
		for (int i = 0; i < numEvents; i++) {
			if (eventName.equalsIgnoreCase(current.eventName)) {
				System.out.println(index);
				break;
			} else {
				
				// makes sure that null pointers dont get thrown
				if (current.next == null) {
					System.out.println("Event Does not Exist Please Try Again");
					exists = false;
				} else {
					index++;
					current = current.next;
					
				}
			}
		}

		
		current = head;
		if (exists) {   // only runs this part if the 

			if (index == 0) {
				head = current.next;
			} else {
				int x = 0;

				// iterates through the list till it gets to the index
				while (x < index - 1) {
					current = current.next;
					x++;
				}
				
				// replaces the node with the next node
				current.next = current.next.next;
			}
			System.out.println("Event has Been Removed");
			numEvents--;  // subtracts from the total 
		}
	}

	
	@Override
	public void changeEvent(String eventName, String date, String time) {
		
		// resets exists back to true
		exists = true;
		Node current = head;
		int index = 0;
		
		// iterates through the linked list once trying to find the index of the event 
		for (int i = 0; i < numEvents; i++) {
			if (eventName.equalsIgnoreCase(current.eventName)) {
				break;
			} else {
				if (current.next == null) {
					System.out.println("Event Does Not Exist Please Try Again");
					exists = false;
				} else {
					index++;
					current = current.next;
				}
			}
		}
		

		current = head;
		
		// only runs this part when the node with that eventName exists
		if (exists) {
			int x = 0;
			
			// replaces the data in that node to the new data
			while (x < numEvents) {
				if (x == index) {
					System.out.println("=");
					current.eventName = eventName;
					current.date = date;
					current.time = time;
					break;
				}
				
				current = current.next;
				x++;
			}

			System.out.println("Event has been changed");
		}
	}

	// lists the events that are stored in the linked list by adding them into a string 
	@Override
	public String listEvents() {

		listEvents = "-----------------------------------------------";

		Node current = head;
		// iterates through the list adding all of the elements to the string
		for (int i = 0; i < numEvents; i++) {
			listEvents = listEvents + "\n" + current.eventName + "\t" + current.date + "\t" + current.time;
			current = current.next;
		}

		// returns the String to the main class
		return listEvents;
	}
	
	
}

// creating a Node class for the Linked list
class Node {
	String eventName;
	String date;
	String time;
	Node next = null;
	
	public Node() {}
	
	// setting the values the are stored in the Node Class to the ones that are passed in through the constructor
	public Node(String eventName, String date, String time) {
		this.eventName = eventName;
		this.date = date;
		this.time = time;
	}
	
}
